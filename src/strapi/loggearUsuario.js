import axios from 'axios';
import url from './URL';
import setupUsuario from './setupUsuario';

async function loggearUsuario({email, password}){
    const respuesta = await axios
    .post(`${url}/auth/local`, {
            identifier: email,
            password
        })
    .catch(error => console.log(error));
    if(respuesta){
        setupUsuario(respuesta);
    }
    return respuesta;
}

export default loggearUsuario;