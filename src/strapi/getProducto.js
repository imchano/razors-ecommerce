import url from './URL';

export default async() => {
    const respuesta = await fetch(`${url}/productos`).catch(error =>
        console.error(error)
        );
    const productos = await respuesta.json();
    if(productos.error){
        return null;
    }
    return productos;
};