import {setUsuarioGuardado, setUsuario} from '../stores/usuario';

function setupUsuario(respuesta){
    const {jwt} = respuesta.data;
    const {username} = respuesta.data.user;
    const usuario = {username, jwt};
    setUsuarioGuardado(usuario);
    setUsuario(usuario);
}

export default setupUsuario;