import axios from 'axios';
import url from './URL';

async function enviarOrden({nombre, total, items, tokenStripe, tokenUsuario}){
    const respuesta = await axios.post(`${url}/ordens`, {nombre, total, items, tokenStripe}, {headers:{Authorization: `Bearer ${tokenUsuario}`}})
    .catch(error => console.log(error));
    return respuesta;
}

export default enviarOrden;