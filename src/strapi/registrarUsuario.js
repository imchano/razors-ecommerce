import axios from 'axios';
import url from './URL';
import setupUsuario from './setupUsuario';

async function registrarUsuario({email, password, username}) {
    const respuesta = await axios
    .post(`${url}/auth/local/register`, {
            email,
            password,
            username
        })
    .catch(error => console.log(error));
    if(respuesta) {
        setupUsuario(respuesta);
    }
    return respuesta;
}

export default registrarUsuario;