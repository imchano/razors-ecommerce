export default [
  {
    id: 1,
    titulo: "razor blade",
    precio: 1.99,
    imagen: "/assets/product-images/product-1.png",
    cantidad: 1
  },
  {
    id: 2,
    titulo: "Gillette Razor",
    precio: 9.99,
    imagen: "/assets/product-images/product-2.png",
    cantidad: 1
  },
  {
    id: 3,
    titulo: "Barber Razor",
    precio: 7.99,
    imagen: "/assets/product-images/product-3.png",
    cantidad: 1
  }
];
