//store para items del carrito
import {writable, derived} from 'svelte/store';

// dummy data
// import localCart from '../localCart';

//carrito
const storeCarrito = writable(getStorageCarrito());

export default storeCarrito;

//total del carrito
export const totalCarrito = derived(storeCarrito, ($storeCarrito)=>{
    let totalCarrito = $storeCarrito.reduce((acc, curr)=>{
        return (acc += curr.cantidad * curr.precio);
    }, 0);
    return parseFloat(totalCarrito.toFixed(2));
})

//funciones locales
const remover = (id, items) =>{
    return items.filter(item => item.id !== id);
}

const modificarCantidad = (id, items, accion) =>{
    return items.map(item =>{
        let nuevaCantidad;
        if (accion === 'aum') {
            nuevaCantidad = item.cantidad + 1;
        }
        else if (accion === 'reduc') {
            nuevaCantidad = item.cantidad -1;
        }
        else {
            nuevaCantidad = item.cantidad;
        }
        return item.id === id? {...item, cantidad: nuevaCantidad} : {...item};

    });
}

//funciones globales
export const removerItem = id =>{
    storeCarrito.update(valorStore =>{
        return remover(id, valorStore);
    })
}

export const aumentarCantidad = id =>{
    storeCarrito.update(valorStore =>{
        return modificarCantidad(id, valorStore, 'aum');
    })
}

export const reducirCantidad = (id, cantidad) =>{
    storeCarrito.update(valorStore =>{
        let carrito;
        if (cantidad === 1) {
            carrito = remover(id, valorStore);
        }
        else {
            carrito = modificarCantidad(id, valorStore, 'reduc');
        }
        return [...carrito];
    });
}

export const agregarCarrito = producto =>{
    storeCarrito.update(valorStore =>{
        const {id, imagen, titulo, precio} = producto;
        let item = valorStore.find(item => item.id === id);
        let carrito;
        if (item){
            carrito = modificarCantidad(id, valorStore, 'aum');
        }
        else {
            let nuevoItem = {id, imagen, titulo, precio, cantidad: 1};
            carrito = [...valorStore, nuevoItem];
        }
        return carrito;
    })
}
//almacenamiento local
function getStorageCarrito(){
    return localStorage.getItem('carrito')?JSON.parse(localStorage.getItem('carrito')):[];
}

export function setStorageCarrito(valoresCarrito){
    localStorage.setItem('carrito', JSON.stringify(valoresCarrito));
}