import {writable} from 'svelte/store';

const storeGlobal = writable({
    sidebar: false,
    carrito: false,
    alerta: false,
    textoAlerta: 'texto default',
    dangerAlerta: false
});

const store = {
    subscribe: storeGlobal.subscribe,
    toggleItem: (item, valor, textoAlerta='texto default', dangerAlerta=false) =>{
        if(item === 'alerta'){
            storeGlobal.update(valoresStore =>{
            return {...valoresStore, [item]:valor, textoAlerta, dangerAlerta};
            });
        }
        else{
            storeGlobal.update(valoresStore =>{
            return {...valoresStore, [item]:valor};
            });
        }
    }
}

export default store;