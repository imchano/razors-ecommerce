import {writable} from 'svelte/store';

//jwt: json web token
const storeUsuario = writable(getUsuarioGuardado());

function getUsuarioGuardado() {
    return localStorage.getItem('usuario')
    ?JSON.parse(localStorage.getItem('usuario')):{username: null, jwt: null};
}

export function setUsuarioGuardado(usuario) {
    localStorage.setItem('usuario', JSON.stringify(usuario));
}

//seteo de la store Usuario
export function setUsuario(usuario){
    storeUsuario.set(usuario);
}

export function logoutUsuario(){
    localStorage.clear();
    storeUsuario.set({username:null, jwt:null});
}
export default storeUsuario;