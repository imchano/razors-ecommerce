import {writable, derived} from 'svelte/store';

// productos locales
// import productosLocales from '../localProducts';
// const store = writable(productoSimplificado([...productosLocales]));

//productos strapi
import productosStrapi from '../strapi/getProducto';
import url from '../strapi/URL';

const store = writable([], () => {
    setProductos();
    return () => {};
});

async function setProductos() {
    let productos = await productosStrapi();
    if(productos){
        productos = productoSimplificado(productos);
        store.set(productos);
    }
}

//producto simplificado
function productoSimplificado(lista){
    return lista.map(item=>{
        let imagen = item.imagen.url;
        // let imagen = `${url}${item.imagen.url}`;
        return {...item, imagen};
    });
}

//store destacados
export const storeDestacados = derived(store, $destacados => {
   return $destacados.filter(item => item.destacado === true);
})

// store exportado con productos locales
export default store;